local random_exits = function(exits)
  local s = ""
  for i=1,math.random(2,3) do
    local e = exits[math.random(5)]
    s = s .. e
  end
  return s
end

local fix_one_way = function(m)
  for y,row in ipairs(m) do
    for x,exits in ipairs(row) do
      local r = m[y][x+1]
      if(r and string.find(r, "l")) then
        m[y][x] = m[y][x] .. "r"
      end
      local l = m[y][x-1]
      if(l and string.find(l, "r")) then
        m[y][x] = m[y][x] .. "l"
      end
      local u = m[y-1] and m[y-1][x]
      if(u and string.find(u, "d")) then
        m[y][x] = m[y][x] .. "u"
      end
      local d = m[y+1] and m[y+1][x]
      if(d and string.find(d, "u")) then
        m[y][x] = m[y][x] .. "d"
      end
    end
  end
end

local random_maze = function(w, h)
  local m = {start={1,1}, finish={w,h}}
  for y=1,h do
    local r = {}
    local exits = {"u", "d", "l", "r", ""}
    for x=1,w do
      if(y == 1) then exits[1] = "" end
      if(y == h) then exits[2] = "" end
      if(x == 1) then exits[3] = "" end
      if(x == w) then exits[4] = "" end
      table.insert(r, random_exits(exits))
    end
    table.insert(m, r)
  end
  fix_one_way(m)
  return m
end

return {new = random_maze}

