local lume = require("polywell.lume")
local has_lfs = false -- pcall(require, "lfs") -- optional dependency

local function fs_for(base_path)
   local get_path = function(path)
      if(path:find("/") == 1 or path:find("*") == 1) then return path end
      return base_path .. "/" .. path
   end

   local fs = {}
   local mt = {
      __separator = "/",
      __index = function(_, path)
         if(path:find("*") == 1) then
            -- TODO: starting with * means that reaching into subdirs strips *
            if(love.filesystem.isDirectory(path:sub(2))) then
               return fs_for(base_path .. "/" .. path:sub(2))
            else
               return love.filesystem.read(path:sub(2))
            end
         end
         local f = io.open(path, "r")
         if(not f) then return nil end
         local _,_,code = f:read(1)
         f:close()
         if(code == 21) then -- directory
            return fs_for(get_path(path))
         else
            return table.concat(lume.array(io.lines(get_path(path))), "\n")
         end
      end,
      __newindex = function(_, path, contents)
         if(path:find("*") == 1) then
            local paths = lume.split(path:sub(2), "/")
            for i,dir in ipairs(paths) do
               if(i < #paths) then love.filesystem.mkdir(dir) end
            end
            return love.filesystem.write(path:sub(2), contents)
         end
         if(contents) then
            local f = io.open(get_path(path), "w")
            f:write(contents)
            f:close()
         end
      end,
      __pairs = function()
         -- without LFS, it will be unable to provide file open completion
         if(has_lfs) then
            return require("lfs").dir(base_path)
         else
            return pairs({})
         end
      end,
   }
   setmetatable(fs, mt)
   return fs
end

return fs_for
