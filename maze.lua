local editor = require("polywell")
local random = require("random")
local solver = require("solver")
local lume = require("lib.lume")

local maze = dofile("mazes/1.lua")
local size, path = 32, nil
local player = {x=maze.start[1], y=maze.start[2]}
w, h = 24, 16

local draw_cell = function(x, y, exits)
   love.graphics.setColor(255,255,255)
   if(not string.find(exits, "u")) then
      love.graphics.line(x*size,y*size,(x+1)*size,y*size)
   end
   if(not string.find(exits, "d")) then
      love.graphics.line(x*size,(y+1)*size,(x+1)*size,(y+1)*size)
   end
   if(not string.find(exits, "l")) then
      love.graphics.line(x*size,y*size,x*size,(y+1)*size)
   end
   if(not string.find(exits, "r")) then
      love.graphics.line((x+1)*size,y*size,(x+1)*size,(y+1)*size)
   end
end

local draw_path = function(path)
   love.graphics.push()
   love.graphics.translate(size/2, size/2)
   love.graphics.scale(size)
   love.graphics.setLineWidth(0.3)
   for i,xy in ipairs(path) do
      if(i > 1) then
         love.graphics.line(path[i-1][1], path[i-1][2], unpack(xy))
      end
   end
   love.graphics.pop()
end

local draw = function()
   love.graphics.setLineWidth(4)
   for y,row in ipairs(maze) do
      for x,exits in ipairs(row) do
         draw_cell(x, y, exits)
      end
   end
   love.graphics.setColor(25,25,255)
   love.graphics.circle("fill", maze.finish[1]*size+size/2,
                        maze.finish[2]*size+size/2, size/2)
   love.graphics.setColor(25,255,25)
   love.graphics.circle("fill", player.x*size+size/2,
                        player.y*size+size/2, size/2)
   if(path) then draw_path(path) end
end

editor.define_mode("maze", "default", {draw=draw})

local allowed = function(dir)
   return string.find(maze[player.y][player.x], dir)
end

editor.bind("maze", "up", function()
               if(allowed("u")) then player.y = player.y - 1 end end)
editor.bind("maze", "down", function()
               if(allowed("d")) then player.y = player.y + 1 end end)
editor.bind("maze", "left", function()
               if(allowed("l")) then player.x = player.x - 1 end end)
editor.bind("maze", "right", function()
               if(allowed("r")) then player.x = player.x + 1 end end)

editor.bind("maze", "escape", love.event.quit)
editor.bind("maze", "ctrl-o", editor.find_file)

editor.bind("maze", "p", function() path = solver(maze, player) end)

require("polywell.config.console")
local close = function() editor.change_buffer(editor.last_buffer()) end
editor.bind("maze", "ctrl-return", lume.fn(editor.change_buffer, "*console*"))
editor.bind("console", "escape", close)
editor.bind("console", "ctrl-return", close)

local random_maze = function()
   local new_maze = random.new(w, h)
   player.x, player.y = new_maze.start[1], new_maze.start[2]
   while(not solver(new_maze, player)) do
      new_maze = random.new(w, h)
   end
   maze, path = new_maze, nil
   player.x, player.y = maze.start[1], maze.start[2]
end

editor.bind("maze", "m", random_maze)

editor.bind("maze", "escape", love.event.quit)

return {random_maze = random_maze}
