local lume = require("lib.lume")
local inspect = require("lib.inspect")

local function solve(m, player, to_visit, path)
   if(not path) then path = {} end
   if(not to_visit) then
      to_visit = {}
      for y,row in ipairs(m) do
         to_visit[y] = {}
         for x in ipairs(row) do
            to_visit[y][x] = lume.set(lume.array(m[y][x]:gmatch(".")))
         end
      end
   end

   if(player.x==m.finish[1] and player.y==m.finish[2]) then return path end
   if(#to_visit[player.y][player.x] == 0 and #path == 0) then return false end

   if(#to_visit[player.y][player.x] == 0) then -- backtrack
      player.x, player.y = unpack(table.remove(path))
      return solve(m, player, to_visit, path)
   else
      table.insert(path, {player.x, player.y})
      local dir = table.remove(to_visit[player.y][player.x], 1)
      if(dir == "u") then player.y = player.y - 1
      elseif(dir == "d") then player.y = player.y + 1
      elseif(dir == "l") then player.x = player.x - 1
      elseif(dir == "r") then player.x = player.x + 1 end
      return solve(m, player, to_visit, path)
   end
end

return solve
