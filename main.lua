local polywell = require("polywell")
local fs_for = require("polywell.fs")

pp = function(x) print(require("lib.inspect")(x)) end

love.keyreleased = polywell.keyreleased
love.keypressed = polywell.keypressed
love.textinput = polywell.textinput
love.wheelmoved = polywell.wheelmoved
love.mousepressed = polywell.mousepressed
love.mousereleased = polywell.mousereleased
love.mousemoved = polywell.mousemoved
love.mousefocus = polywell.mousefocus
love.draw = polywell.draw

love.load = function(args)
   love.graphics.setFont(love.graphics.newFont("polywell/inconsolata.ttf", 14))
   love.keyboard.setTextInput(true)
   love.keyboard.setKeyRepeat(true)
   love.math.setRandomSeed(os.time())
   local maze = require("maze")
   require("polywell.config.edit")
   require("polywell.config.lua_mode")
   require("polywell.config.emacs_keys")

   polywell.fs = fs_for(".")
   polywell.open("*maze*", nil, "maze")
   maze.random_maze()
end
